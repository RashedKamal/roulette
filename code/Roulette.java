import java.util.Scanner;

public class Roulette{
    public static int calculateUserWinnings(int betNum, int betAmount, int result){ //helper method
        if (betNum == result){
            return betAmount * 35; // if they win, get 35x bet amount
        }
        else {
            return -betAmount; // if they lose, loses money equal to bet amount
        }
    }
    public static void main (String[] args){
        RouletteWheel rouletteWheel = new RouletteWheel();
        int userMoney = 1000; // every user starts with 1000$.
        int betAmount = 0;
        Scanner s = new Scanner(System.in);
        while (userMoney > 0){
            System.out.println("Current money: $" + userMoney);
            System.out.println("Would you like to make a bet? (Yes or No): ");
            String choice = s.nextLine();
            if (choice.equalsIgnoreCase("yes")){ 
                while (betAmount <=0 || betAmount > userMoney){ // validates input
                    System.out.println("Enter your bet amount (valid range: 1-" + userMoney + "): ");
                    betAmount = s.nextInt();
                    s.nextLine();
                }
                System.out.println("Enter the number (0-36) to bet on: ");
                int betNum = s.nextInt();
                s.nextLine();
                rouletteWheel.spin(); // chooses random num
                int result = rouletteWheel.getValue(); // obtains random num
                System.out.println("The number you landed on is: " + result);

                int winnings = calculateUserWinnings(betNum, betAmount, result);
                userMoney += winnings; // user's new money amount after their winnings/losses

                if (winnings > 0){
                    System.out.println("You won! You now have $" + userMoney);
                }
                else {
                    System.out.println("You lost :( You now have $" + userMoney);
                }
                
                betAmount = 0; // reset to allow user to enter new bet amount
            }
            else if (choice.equalsIgnoreCase("no")){
                break;
            }
        }
        System.out.println("Game over. Final amount: " + userMoney); // after user goes below $0
    }
}